package org.example.适配器;

public class SomeBodyElse implements TwoParam {
    @Override
    public void doIt(int x, int y) {
        System.out.println("SomeBodyElse doIt with " + x + y);
    }
}
