package org.example.适配器;

public class Two2ThreeAdapter implements ThreeParam {

    TwoParam toAdaptObj;

    public Two2ThreeAdapter(TwoParam toAdaptObj) {
        this.toAdaptObj = toAdaptObj;
    }

    @Override
    public void doIt(int x, int y, int z) {
        System.out.println("3 params adapted to 2 params");
        toAdaptObj.doIt(x, y);
    }
}
