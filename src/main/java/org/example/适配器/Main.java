package org.example.适配器;


/**
 * <h2>适配器模式的适用场景</h2>
 * <p>适配器模式适用于以下几种情况：</p>
 * <ul>
 *     <li><strong>已有类接口不符合需求：</strong>当现有类的接口与需要使用的接口不兼容时，可以使用适配器模式进行转换。</li>
 *     <li><strong>需要使用不兼容的第三方类：</strong>在使用第三方库或类时，如果其接口不符合项目需求，可以通过适配器将其适配。</li>
 *     <li><strong>在不修改现有代码的情况下引入新功能：</strong>如果需要在现有系统中添加新功能，但又不想修改已有代码，可以使用适配器来实现。</li>
 *     <li><strong>多种不同接口类的协作：</strong>当系统中存在多个不同接口的类，需要它们能够协同工作时，可以使用适配器模式来统一接口。</li>
 *     <li><strong>统一不同数据源接口：</strong>在处理多个数据源时，如果它们的接口不一致，可以使用适配器模式来提供统一的访问接口。</li>
 * </ul>
 * <p>适配器模式提高了系统的灵活性和可扩展性，解决了接口不兼容的问题。</p>
 */
public class Main {
    public static void main(String[] args) {
        SomeBody someBody = new SomeBody();
        SomeBodyElse someBodyElse = new SomeBodyElse();
        new Two2ThreeAdapter(someBody).doIt(1, 2, 3);
        new Two2ThreeAdapter(someBodyElse).doIt(1, 2, 3);
    }
}
