package org.example.装饰器;

/**
 * 装饰器2
 */
public class D2 extends Decorator {

    public D2(ShowAble showAble) {
        super(showAble);
    }

    @Override
    public void show() {
        System.out.println("some D2");
        super.show();
    }
}
