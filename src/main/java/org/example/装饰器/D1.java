package org.example.装饰器;

/**
 * 装饰器1
 */
public class D1 extends Decorator {
    public D1(ShowAble showAble) {
        super(showAble);
    }

    @Override
    public void show() {
        System.out.println("some D1");
        super.show();
    }
}
