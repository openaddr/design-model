package org.example.装饰器;

/**
 * 被装饰者
 */
public class Girl implements ShowAble{
    @Override
    public void show() {
        System.out.println("Girl's face");
    }
}
