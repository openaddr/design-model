package org.example.装饰器;

public abstract class Decorator implements ShowAble{
    ShowAble showAble; //被装饰者

    public Decorator (ShowAble showAble){
        this.showAble = showAble;
    }

    @Override
    public void show() {
        showAble.show();
    }
}
